var angular = require('angular');

var patientsModule = require('./app/patients/index');
require('angular-ui-router');
require('angular-ui-bootstrap');
var routesConfig = require('./routes');

var main = require('./app/main');
var title = require('./app/title');

require('./index.scss');

angular
  .module('app', [patientsModule, 'ui.bootstrap', 'ui.router'])
  .config(routesConfig)
  .component('app', main)
  .component('fountainTitle', title);
