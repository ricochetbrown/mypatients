module.exports = {
  template: require('./patient.html'),
  bindings: {
    patient: '<'
  },
  controller: PatientController
};

/** @ngInject */
function PatientController($uibModal, $document) {
  var $ctrl = this;

  $ctrl.formats = ['shortDate'];
  $ctrl.format = $ctrl.formats[0];
  $ctrl.popup1 = {
    opened: false
  };

  $ctrl.genderDetermination = function (gender) {
    return gender === 'Male' ? 'fa fa-mars' : 'fa fa-venus';
  };

  $ctrl.open1 = function () {
    $ctrl.popup1.opened = true;
  };

  $ctrl.setDate = function (year, month, day) {
    $ctrl.patient.dateOfBirth = new Date(year, month, day);
  };

  $ctrl.animationsEnabled = true;

  $ctrl.open = function (size, parentSelector) {
    var parentElem = parentSelector ?
      angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
    var modalInstance = $uibModal.open({
      animation: $ctrl.animationsEnabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      controllerAs: '$ctrl',
      size: size,
      appendTo: parentElem,
      resolve: {
        patient: function () {
          return $ctrl.patient;
        }
      }
    });

    modalInstance.result.then(function (patient) {
      $ctrl.patient = patient;
    });
  };
}

function ModalInstanceCtrl($uibModalInstance, patient) {
  var $ctrl = this;
  $ctrl.patient = patient;

  $ctrl.ok = function () {
    $uibModalInstance.close($ctrl.patient);
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
}
