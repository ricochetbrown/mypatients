module.exports = {
  template: require('./patients.html'),
  controller: PatientsController
};

/** @ngInject */
function PatientsController($http) {
  var vm = this;

  $http
    .get('app/patients/patients.json')
    .then(function (response) {
      vm.patients = response.data;
    });
}
