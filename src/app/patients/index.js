var angular = require('angular');

var patient = require('./patient');
var patients = require('./patients');

var patientsModule = 'patients';

module.exports = patientsModule;

angular
  .module(patientsModule, [])
  .component('fountainPatient', patient)
  .component('fountainPatients', patients);
