var angular = require('angular');
require('angular-mocks');
var patients = require('./patients');

var patientsJson = [
  {
    "key": "evlo",
    "title": "Mr.",
    "logo": "http://a.espncdn.com/combiner/i?img=/i/headshots/mlb/players/full/28639.png&w=350&h=254",
    "firstName": "Evan",
    "lastName": "Longoria",
    "dateOfBirth": "10/7/85",
    "gender": "Male",
    "address": {
      "street": "177 Rays Way",
      "city": "St. Petersburg",
      "state": "FL",
      "zip": "33710"
    },
    "occupation": "Third Baseman",
    "phones": {
      "home": "727-777-7777",
      "cell": "727-777-7777"
    },
    "email": "longo@rays.com"
  },
  {
    "key": "jawi",
    "title": "Mr.",
    "logo": "http://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/2969939.png&w=350&h=254",
    "firstName": "Jameis",
    "lastName": "Winston",
    "dateOfBirth": "January 6, 1994 (age 23)",
    "gender": "Male",
    "address": {
      "street": "177 Bucs Way",
      "city": "Tampa",
      "state": "FL",
      "zip": "33801"
    },
    "occupation": "Quarterback",
    "phones": {
      "home": "727-888-8888",
      "cell": "727-888-8888"
    },
    "email": "winston@bucs.com"
  },
  {
    "key": "char",
    "title": "Mr.",
    "logo": "http://a.espncdn.com/combiner/i?img=/i/headshots/mlb/players/full/31003.png&w=350&h=254",
    "firstName": "Chris",
    "lastName": "Archer",
    "dateOfBirth": "September 26, 1988 (age 28)",
    "gender": "Male",
    "address": {
      "street": "177 Rays Way",
      "city": "St. Petersburg",
      "state": "FL",
      "zip": "33710"
    },
    "occupation": "Starting Pitcher",
    "phones": {
      "home": "727-777-7777",
      "cell": "727-777-7777"
    },
    "email": "archer@rays.com"
  }
];

describe('patients component', function () {
  beforeEach(function () {
    angular
      .module('fountainPatients', ['app/patients/patients.html'])
      .component('fountainPatients', patients);
    angular.mock.module('fountainPatients');
  });
  it('should render 3 elements <fountain-patient>', angular.mock.inject(function ($rootScope, $compile, $httpBackend) {
    $httpBackend.when('GET', 'app/patients/patients.json').respond(patientsJson);
    var element = $compile('<fountain-patients></fountain-patients>')($rootScope);
    $httpBackend.flush();
    $rootScope.$digest();
    var patients = element.find('fountain-patient');
    expect(patients.length).toEqual(3);
  }));
});
