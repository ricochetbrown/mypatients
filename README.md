# README #

### Patient Dashboard Coding Challenge ###

### How do I get set up? ###

* cd to your repo path
* Run "npm install" in command line
* Run "npm run serve" to launch the app
* View application at localhost:3000
* View Browsersync details at localhost:3001
* Run unit tests with "gulp test"